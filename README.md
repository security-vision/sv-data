# sv-data

Build a JSON dataset using the [securityvision.io/wiki](https://securityvision.io/wiki) API.

Work in progress workflow:

```
-> for each category
	-> get list of pages
	-> for each page
		-> get list of properties
		-> for each property
			-> parse property
		-> return parsed_properties
		-> for each parsed_property
			-> if link
				-> if target doesn't extists in targets
					-> create target and add to targets
				-> parse link
				-> add link to links
			-> else property
				-> add property to page
		add page to nodes
		add links to links
```

Output a file with the following structure:

```json
{
	"nodes": [
		{
			"id": 123,
			"name": "A page of the wiki",
			"some properties": "...other properties"
		}
	],
	"links": [
		{
			"source": 123,
			"target": 456,
			"name": "relation type"
		}
	]
}
```

## Setup

Clone the repo and create a virtual environment

```
python -m venv venv
source venv/bin/activate
```

Install the requirements from `setup.py`

```
python setup.py install
```

Run `main.py`

```
python sv-data/main.py
```

## Emerging tools

Working on this code we get some insights about the data structure of the wiki, its flaws and some possible approaches to address them. Here is a list of small scripts used in this piece of code that could be deployed as standalone tools to help organizing and cleaning the dataset.

-   List of cities with missing coords, that could be generalized as list of pages with missing property. Given a property name, it returns a list of pages where this property is missing. For each page the correspondent url is printed, to facilitate data entry.
-   Consider a common format for city names. US cities for example come with their state (ie: `Pasadena (CA)`).

## Notes

-   What to do when multiple cities are present in a node? Is it better to represent a city as property or as node? I would say node linked to different entities! (At the moment we are just keeping the first city in the list)
-   Some editorial work is necessary for the `active` and `passive` forms for link relations (ie: `used_by` that becomes `Used by` for passive, `Using` for active)
