import logging

logger = logging.getLogger("utils")


def parse_property(key, values):
    """Extract and clean property name and values"""
    values = [x["item"].rsplit("#0##", 1)[0].replace("_", " ") for x in values]
    if len(values) == 1:
        values = values[0]

    parsed = values
    name = key

    match key:
        case "is_unknown":
            # Convert value to Boolean
            name = "unknown"
            parsed = values == "t"
        case "geolocation":
            name = "coords"
            parsed = {"lat": values.rsplit(",")[0], "lon": values.rsplit(",")[1]}
        case "_uri":
            name = "links"
        case "address":
            parsed = "\n".join(values)
        case "city":
            # temporary consider only first city
            parsed = values[0] if type(values) == list else values

    return name, parsed


def integrate_coords(properties, cities):
    """Integrate missing coordinates looking up to a dataset of cities"""
    if "coords" not in properties and "city" in properties:
        logger.warning(
            f'Missing coordinates info for "{properties["city"]}", trying to integrate from cities list'
        )
        city = properties["city"]
        if city in cities and "coords" in cities[city]:
            properties["coords"] = cities[city]["coords"]
            logger.debug("Found coordinates!")
        else:
            logger.warning("No coordinates available")
    return properties


def parse_data(data):
    properties = {}
    if data["query"]["data"]:
        for el in data["query"]["data"]:
            # transform property name to lowercase
            key = el["property"].lower()

            # Filter out irrelevant properties
            if key in [
                "cited_references",
                "is_in_greens_report_2021",
                "has_full_name",
                "has_event",
                "importance",
                "_ask",
                "_inst",
                "_mdat",
                "_skey",
                "_sobj",
                "fullname",
                "__sci_cite_reference",
                "deployment_status",
            ]:
                continue

            # Consider only following properties
            # if key not in [
            #     'title',
            #     'country',
            #     'city',
            #     'geolocation',
            #     '_uri'
            # ]:
            #     continue

            # Parse values

            # if key in [
            #     "managed_by",
            #     "used_by",
            #     "dataset_used",
            #     "deployment_type",
            #     "software_deployed",
            #     "related_institutionsa",
            #     "name",
            # ]:
            #     link = parse_link(page["pageid"], key, el["dataitem"])
            #     links.extend(link)
            # else:
            name, values = parse_property(key, el["dataitem"])
            properties[name] = values
    return properties
