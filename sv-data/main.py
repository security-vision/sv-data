import json
import logging
import queries
import session
import utils
import uuid


def get_data(categories):
    """Build dataset with a list of nodes and links from a list of categories."""
    collection = {"nodes": [], "links": []}

    nodes = {}
    links = []

    cities = queries.get_cities(base_url, SESSION)

    for category in categories:
        pages = queries.get_pages_for_category(category, base_url, SESSION)
        for page in pages:
            name = page["title"]
            if name not in nodes:
                id = page["pageid"]
            else:
                id = nodes[name]["id"]

            node = {"id": id, "name": name, "category": category}

            if name in nodes:
                node = {**node, **nodes[name]}

            nodes[name] = node

            data = queries.get_data_for_page(page, base_url, SESSION)
            properties = utils.parse_data(data)
            properties = utils.integrate_coords(properties, cities)

            for key, value in properties.items():

                def add_link(key, id, name, active=False):
                    """Check if link entity is already in nodes, otherwise create it"""
                    if name not in nodes:
                        logger = logging.getLogger("links")
                        node_id = uuid.uuid4().time_low
                        logger.warning(
                            f'"{name}" doesn\'t extist. Generting node with id {node_id}'
                        )
                        nodes[name] = {
                            "id": node_id,
                            "name": name,
                        }
                    links.append(
                        {
                            "edge_type": key,
                            "from": nodes[name]["id"] if active else id,
                            "to": id if active else nodes[name]["id"],
                        }
                    )

                # Check if link
                # there are two categories of link: `active` and `passive`
                # `active` are for example `dataset_used`, `software_deployed`, ... - ex: item deploys software id
                # `passive` are for example `managed_by`, `used_by`, `developed_by` - ex: id is managed by item

                if key in [
                    "managed_by",
                    "used_by",
                    "developed_by",
                    "owning_institution",
                    "is_department_of",
                    "custodian_institution",
                    "related_institutions",
                    "dataset_used",
                    "software_deployed",
                ]:
                    active = key in [
                        "related_institutions",
                        "dataset_used",
                        "software_deployed",
                    ]

                    if type(value) == list:
                        for item in value:
                            add_link(key, id, item, active)
                    else:
                        add_link(key, id, value, active)

                else:
                    # else push to node properties
                    node[key] = value

            nodes[name] = node

    collection["nodes"] = list(nodes.values())
    collection["links"] = links

    with open("tmp/data.json", "w", encoding="utf-8") as f:
        logger.info("Writing to file tmp/data.json")
        json.dump(collection, f)


logging.basicConfig(filename=".log", encoding="utf-8", level=logging.INFO)
logger = logging.getLogger()

# Global variables
base_url = "https://www.securityvision.io/wiki/api.php"
SESSION = session.init(base_url)

CITIES = {}
default_categories = [
    "Deployments",
    "Institution",
    "Dataset",
]

if __name__ == "__main__":
    get_data(default_categories)
